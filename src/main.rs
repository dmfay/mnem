extern crate dirs;
extern crate skim;

use std::cmp::Ordering;
use std::collections::HashMap;
use std::collections::HashSet;
use lazy_static::lazy_static;
use maplit::hashmap;
use regex::Regex;
use structopt::StructOpt;
use skim::prelude::*;

#[derive(StructOpt)]
struct Mnem {
}

struct Reflection {
    instances: HashSet<String>,
    count: i32
}

lazy_static! {
    static ref IS_EXP:Regex = Regex::new(r#": \d{10}:\d+;"#).unwrap();
}

fn get_history_content() -> String {
    let mut histfile = dirs::home_dir().unwrap();

    for name in vec!(".bash_history", ".histfile") {
        histfile.push(name);

        if histfile.is_file() {
            return std::fs::read_to_string(histfile).expect("history file not found!");
        }

        histfile.pop();
    }

    return "".to_string();
}

fn process_history() -> HashMap<String, Reflection> {
    let complex_cmd_lengths:HashMap<&str, i32> = hashmap!{
        // TODO more complex patterns
        "sudo" => 2,
        "systemctl" => 2,
        "aws" => 3,
        "git" => 2,
        "cargo" => 2,
        "npm" => 2,
        "heroku" => 2
    };

    let dquotes = Regex::new(r#""(?:[^"\\]|\\.)*""#).unwrap();
    let quotes = Regex::new(r#"'(?:[^'\\]|\\.)*'"#).unwrap();

    let mut history = HashMap::new();

    let content = get_history_content();

    for line in content.lines() {
        let cmd = IS_EXP.split(line).last().unwrap();
        let de_dquoted = dquotes.replace_all(cmd, "<quoted>");
        let dequoted = quotes.replace_all(de_dquoted.as_ref(), "<quoted>");

        // split by space
        // TODO escaped spaces -- regex?
        let mut split = dequoted.split(" ");
        let mut vec = Vec::new();
        let mut switches = Vec::new();
        let mut was_switch = false;
        let mut idx = 1;

        let first = split.next().unwrap();

        let max_words = *complex_cmd_lengths.get(first).unwrap_or(&1);

        vec.push(first.to_string());

        while let Some(token) = split.next() {
            // TODO inline env vars
            if token == "|" {
                // pipe into a new command, reset tracking
                // TODO will eventually need its own switch vec
                idx = 0;

                vec.push(token.to_string());
            } else if token.starts_with("-") {
                // TODO not perfect -- e.g. datediff -f $d1 $d2, git add -p f1 f2 f3
                was_switch = true;

                switches.push(token.to_string());
            } else if was_switch {
                was_switch = false;

                let switch = switches.pop().unwrap();

                switches.push([switch, "<val>".to_string()].join(" "));
            } else {
                idx += 1;

                if idx > max_words {
                    vec.push(format!("<arg{}>", idx - max_words).to_string());
                } else {
                    vec.push(token.to_string());
                }
            }
        }

        // sort switches consistently
        switches.sort();

        // rejoin
        vec.append(& mut switches);

        let processed = vec.join(" ");

        let cmd = IS_EXP.split(line).last().unwrap();

        history.entry(String::from(processed))
            .and_modify(|c: &mut Reflection| {
                c.instances.insert(cmd.to_string());
                c.count += 1;
            }).or_insert(Reflection {
                instances: vec![cmd.to_string()].into_iter().collect(),
                count: 1
            });
    }

    return history;
}

fn pick_template(options:&SkimOptions, sorted:Vec<(&String, &Reflection)>) -> String {
    let (tx_item, rx_item): (SkimItemSender, SkimItemReceiver) = unbounded();

    for r in sorted.iter() {
        let _ = tx_item.send(Arc::new(r.0.to_string()));
    }

    drop(tx_item);

    let skimmed = Skim::run_with(options, Some(rx_item))
        .map(|out| out.selected_items)
        .unwrap_or_else(|| Vec::new());

    let selection = skimmed.iter().last();

    if selection.is_some() {
        let template = selection.unwrap().output().to_string();

        return template;
    }

    return "".to_string();
}

fn pick_instance(options:&SkimOptions, reflection:&Reflection) -> String {
    let (tx_item, rx_item): (SkimItemSender, SkimItemReceiver) = unbounded();

    for i in reflection.instances.iter() {
        let _ = tx_item.send(Arc::new(i.to_string()));
    }

    drop(tx_item);

    let skimmed = Skim::run_with(&options, Some(rx_item))
        .map(|out| out.selected_items)
        .unwrap_or_else(|| Vec::new());

    let selection = skimmed.iter().last();

    if selection.is_some() {
        let instance = selection.unwrap().output().to_string();

        return instance;
    }

    return "".to_string();
}

pub fn main() {
    let options = SkimOptionsBuilder::default()
        .height(Some("33%"))
        .color(Some("bw"))
        .multi(false)
        .build()
        .unwrap();

    let history = process_history();
    let mut sorted:Vec<_> = history.iter().collect();

    sorted.sort_by(|a, b| {
        match b.1.count.cmp(&a.1.count) {
            Ordering::Equal => a.0.cmp(b.0),
            other => other
        }
    });

    println!();

    let template = pick_template(&options, sorted);

    if template != "" {
        let reflection = history.get(&template).unwrap();
        let instance = pick_instance(&options, reflection);
        let cmd = IS_EXP.split(&instance).last().unwrap();
        print!("{}\n", cmd);
    } else {
        println!("aborted");
    }

    println!();
}
