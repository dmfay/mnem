# Mnem

Mnem is an extra-fuzzy shell history searcher.

If you use Bash or zsh, you may already be familiar with the `<Ctrl>-r`/`<Ctrl>-s` "reverse search" functionality that finds matches backwards or forwards through the log of commands you've already entered. Reverse search is great and comes packaged with just about every system you'd want to use it on, but of necessity it works very mechanically with history: only one result at a time, sorted only by time.

![Mnem in use; refer below for details.](img/termtosvg_l9tv9ce0.svg "Mnem usage")

Mnem first shows you reduced forms of the commands you use. Argument and option values are masked so different invocations with the same structure can be counted together, and the whole is sorted by popularity.

Once you've selected an entry in the usual fuzzy-find way (`<Ctrl>-j/<Ctrl>-k` work too, thanks to [skim](https://github.com/lotabout/skim)), Mnem fills another fuzzy finder with your matching invocations. Finally, it prints your history match to stdout for you to copy and paste, reference, or evaluate.

## Installation

### Arch Linux

Install the `mnem` package from the AUR.

### OSX

```
brew tap dmfay/mnem https://gitlab.com/dmfay/homebrew-mnem.git
brew install dmfay/mnem/mnem
```

## Notes

Mnem is in very early stages yet. Contributions are welcome, and if you happen to know how I can output the match to the actual command buffer instead of printing it I _really_ want to know that!
